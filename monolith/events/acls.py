from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

#create helper fxn
def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": city + " " + state
    }
    url = "https://api.pexels.com/vl/search"
    response = requests.get(url, params=params, headers=headers)
    #content = response.content
    parsed_json = json.loads(response.content)
    picture = parsed_json["photos"][0]["src"]["original"]
    return {
        "picture_url": picture
    }

    #what we had
    #resource = requests.get("https://api.pexels.com/v1/search?query={city},{state}}&per_page=1")
    # location = json.loads(response.content)
    # picture = {
    #     "picture_url": location["photos"]["url"]
    # }
    # return picture


def get_lat_long(location):
    """
    Returns the latitude and longitude for the specified locaiton using the openweathermap api
    """

    base_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(base_url, params)
    parsed_json = json.loads(response.content)
    return {
        "latitude": parsed_json[0]["lat"],
        "longitude": parsed_json[0]["lon"]
    }

def get_weather_data(location):
    """
    Returns current weather data for the specified location using the openweathermap api
    """
    lat_long = get_lat_long(location)
    base_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_long["latitude"],
        "lon": lat_long["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    weather_data = {
        "temp": parsed_json["main"]["temp"],
        # weather is an array
        "description": parsed_json["weather"][0]["description"]
    }
    return weather_data




    #what we had
    #def get_weather_data(city, state):
    # response = requests.get("http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{US}&limit={limit}&appid={API key}")
    # location = json.loads(response.content)
    # coordinates = {
    #     "lat": location["lat"],
    #     "long": location["long"]
    # }

    # response_weather = requests.get(https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key})
    # weather = {
    #     "temperature":location["temperature"],
    #     "description": location["description"]
    # }
    # return weather,coordinates
